import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { makeStyles } from "@material-ui/core/styles";
import { styled } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Layout from '../../common/Layout';

const useStyles = makeStyles((theme) => ({
    root: {
        paddingTop: '64px'
    },
    commonImg: {
        height: '457px',
        width: '100%',
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
    },
    inverse: {
        transform: 'rotate( 180deg)'
    },
    titleContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    titleBtn:{
        display: 'flex',
        width: '160px',
        justifyContent: 'space-between'
    },
    [theme.breakpoints.down('xs')]: {
        commonImg: {
            height: '284px',
            width: '100%',
        }
    },
}));


const BasicTarot: React.FC = function BasicTarot() {
    const classes = useStyles();
    const [tarots, setTarots] = React.useState([]);

    React.useEffect(() => {
        
        return () => {

        }
    }, []);

    const createCard = () => {
        var cards = [];
        //建立小牌牌組
        for (var i = 0; i < 56; i++) {
            var suit = i / 14 | 0;
            var level = (i % 14) + 1;
            var card = { suit: suit, level: level };
            cards.push(card);
        }
        //建立大牌牌組
        for (var i = 0; i < 22; i++) {
            var card = { suit: 4, level: i };
            cards.push(card);
        }
        return cards;
    }

    const shuffle = (array: any) => {
        var i = array.length,
            j = 0,
            temp;

        while (i--) {
            j = Math.floor(Math.random() * (i + 1));

            // swap randomly chosen element with current element
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        return array;
    }

    const getClassName = (card: any) => {
        const suit = card.suit;
        let prefix = "";
        const suffix = card.level;
        switch (suit) {
            case 0: prefix = "Pents"; break;
            case 1: prefix = "Swords"; break;
            case 2: prefix = "Cups"; break;
            case 3: prefix = "Wands"; break;
        }
        if (suit < 4 && suffix < 10) {
            return prefix + "0" + suffix;
        } else if (suit < 4 && suffix > 10) {
            return prefix + suffix;
        }
        switch (suffix) {
            case 0: return "theFool";
            case 1: return "theMagician";
            case 2: return "theHighPriestess";
            case 3: return "theEmpress";
            case 4: return "theEmperor";
            case 5: return "theHierophant";
            case 6: return "theLovers";
            case 7: return "theChariot";
            case 8: return "strength";
            case 9: return "theHermit";
            case 10: return "wheelOfFortune";
            case 11: return "justice";
            case 12: return "theHangedMan";
            case 13: return "death";
            case 14: return "temperance";
            case 15: return "theDevil";
            case 16: return "theTower";
            case 17: return "theStar";
            case 18: return "theMoon";
            case 19: return "theSun";
            case 20: return "judgement";
            case 21: return "theWorld";
        }
        return suffix;
    }

    const getTreeCards = () => {
        const cards = shuffle(createCard());
        let tarots = [];
        for (let i = 0; i < 3; i++) {
            const eachCls = [classes.commonImg];
            const isInverse = Math.round(Math.random());
            if (isInverse === 1) {
                eachCls.push(classes.inverse);
            }
            const eachTarot = {
                tarotCls: eachCls.join(' '),
                tarotType: getClassName(cards[i])
            };
            tarots.push(eachTarot);
        }

        setTarots(tarots);

    }

    const clearCards = () => {
        setTarots([]);
    }

    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));

    return (
        <Layout>
        <Container maxWidth="md">
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item xs={12} className={classes.titleContent} >
                        <h1 className="jumbotron-heading">偉特塔羅占卜</h1>
                        <p className="lead text-muted">首先，心裏默念你想問的<b>問題</b>，然後點擊<b>抽牌</b>，接著填寫 <b>問題描述的資料</b></p>
                        <p className="lead text-muted">最後寄給命理老師，由老師來詳細解析</p>
                    <p className={classes.titleBtn}>
                    <Button variant="contained" onClick={getTreeCards} >抽牌</Button>
                    <Button variant="contained" onClick={clearCards} >清空</Button>
                    </p>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                {tarots.map((item, index) => {
                    return (<Grid item xs={4} key={index}>
                        <Item>
                            <div className={item.tarotCls} style={{ backgroundImage: 'url(\'/tarot/' + item.tarotType + '.jpeg\')' }} >

                            </div>
                        </Item>
                    </Grid>
                    )
                })}
            </Grid>
        </Box >
        </Container >
        </Layout>        
    );
}

export default BasicTarot