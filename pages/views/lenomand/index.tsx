import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { makeStyles } from "@material-ui/core/styles";
import { styled } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Layout from '../../common/Layout';

const useStyles = makeStyles((theme) => ({
    root: {
        paddingTop: '64px'
    },
    commonImg: {
        height: '180px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        fontSize: '1.5rem',
        fontWeight: 900
    },
    typeBg:{
        position: 'absolute',
        top:'0',
        left:'0',
        height:'100%',
        width: '100%',
        fontSize: '7rem',
        opacity: 0.05,
        lineHeight: 'unset'
    },
    inverse: {
        transform: 'rotate( 180deg)'
    },
    titleContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    titleBtn: {
        display: 'flex',
        width: '160px',
        justifyContent: 'space-between'
    },
    cardCls:{
     margin: '.2rem'
    },
    [theme.breakpoints.down('xs')]: {
        commonImg: {
            height: '70px',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-around',
            fontSize: '1rem'                
        },
        typeBg:{
            position: 'absolute',
            bottom:'0',
            left:'0',
            height:'100%',
            width: '100%',
            fontSize: '3rem',
            opacity: 0.1,
            lineHeight: '1.1rem'
        },        
    },
}));

const lenormandCards = [
    {
        sort: 1,
        type: '♥',
        color: 'red',
        name: '騎士'
    },
    {
        sort: 2,
        type: '♦',
        color: 'red',
        name: '幸運草'
    },
    {
        sort: 3,
        type: '♠',
        color: 'black',
        name: '船'
    },
    {
        sort: 4,
        type: '♥',
        color: 'red',
        name: '房屋'
    },
    {
        sort: 5,
        type: '♥',
        color: 'red',
        name: '樹'
    },
    {
        sort: 6,
        type: '♣',
        color: 'black',
        name: '雲朵'
    },
    {
        sort: 7,
        type: '♣',
        color: 'black',
        name: '蛇'
    },
    {
        sort: 8,
        type: '♦',
        color: 'red',
        name: '棺木'
    },
    {
        sort: 9,
        type: '♠',
        color: 'black',
        name: '花束'
    },
    {
        sort: 10,
        type: '♦',
        color: 'red',
        name: '鐮刀'
    },
    {
        sort: 11,
        type: '♥',
        color: 'black',
        name: '鞭子'
    },
    {
        sort: 12,
        type: '♦',
        color: 'red',
        name: '小鳥'
    },
    {
        sort: 13,
        type: '♠',
        color: 'black',
        name: '小孩'
    },
    {
        sort: 14,
        type: '♣',
        color: 'black',
        name: '狐狸'
    },
    {
        sort: 15,
        type: '♣',
        color: 'black',
        name: '熊'
    },
    {
        sort: 16,
        type: '♥',
        color: 'red',
        name: '星星'
    },
    {
        sort: 17,
        type: '♥',
        color: 'red',
        name: '鶴'
    },
    {
        sort: 18,
        type: '♥',
        color: 'red',
        name: '小狗'
    },
    {
        sort: 19,
        type: '♠',
        color: 'black',
        name: '塔'
    },
    {
        sort: 20,
        type: '♣',
        color: 'black',
        name: '公園'
    },
    {
        sort: 21,
        type: '♣',
        color: 'black',
        name: '山'
    },
    {
        sort: 22,
        type: '♦',
        color: 'red',
        name: '分岔路'
    },
    {
        sort: 23,
        type: '♣',
        color: 'black',
        name: '老鼠'
    },
    {
        sort: 24,
        type: '♥',
        color: 'red',
        name: '心'
    },
    {
        sort: 25,
        type: '♣',
        color: 'black',
        name: '戒指'
    },
    {
        sort: 26,
        type: '♦',
        color: 'red',
        name: '書'
    },
    {
        sort: 27,
        type: '♠',
        color: 'black',
        name: '信件'
    },
    {
        sort: 28,
        type: '♥',
        color: 'red',
        name: '男人'
    },
    {
        sort: 29,
        type: '♠',
        color: 'black',
        name: '女人'
    },
    {
        sort: 30,
        type: '♠',
        color: 'black',
        name: '百合花'
    },
    {
        sort: 31,
        type: '♦',
        color: 'red',
        name: '太陽'
    },
    {
        sort: 32,
        type: '♥',
        color: 'red',
        name: '月亮'
    },
    {
        sort: 33,
        type: '♦',
        color: 'red',
        name: '鑰匙'
    },
    {
        sort: 34,
        type: '♦',
        color: 'red',
        name: '魚'
    },
    {
        sort: 35,
        type: '♠',
        color: 'black',
        name: '船錨'
    },
    {
        sort: 36,
        type: '♣',
        color: 'black',
        name: '十字架'
    },
];


const Lenormand: React.FC = function Lenormand() {
    const classes = useStyles();
    const [lenormands, setLenormands] = React.useState([]);

    React.useEffect(() => {
       
        return () => {

        }
    }, []);

    const createCard = () => {
        const cards = shuffle(lenormandCards);
        const part1 = cards.slice(0, 9);
        const part2 = cards.slice(9, 18);
        const part3 = cards.slice(18, 27);
        const part4 = cards.slice(27, 36);

        setLenormands([part1, part2, part3, part4]);

    }

    const shuffle = (array: any) => {
        var i = array.length,
            j = 0,
            temp;

        while (i--) {
            j = Math.floor(Math.random() * (i + 1));

            // swap randomly chosen element with current element
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        return array;
    }


    const clearCards = () => {
        setLenormands([]);
    }

    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));


    const items = (childs) => {
        return (childs.map((each: any) => {
            return (

                <Grid item xs={1} key={each.sort}  className={classes.cardCls} >
                    <Item style={{position: 'relative'}} >
                        <div className={classes.typeBg} style={{color: each.color}}>
                        {each.type}
                        </div>
                        <div className={classes.commonImg}  >
                            {each.name} {each.sort}
                        </div>
                    </Item>
                </Grid>
            )
        }
        )
        )
    }

    return (
        <Layout>
            <Container maxWidth="lg">
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} className={classes.titleContent} >
                            <h1 className="jumbotron-heading">雷諾曼卡占卜</h1>
                            <p className="lead text-muted">首先，心裏默念你想問的<b>問題</b>，然後點擊<b>抽牌</b>，接著填寫 <b>問題描述的資料</b></p>
                            <p className="lead text-muted">最後寄給命理老師，由老師來詳細解析</p>
                            <p className={classes.titleBtn}>
                                <Button variant="contained" onClick={createCard} >抽牌</Button>
                                <Button variant="contained" onClick={clearCards} >清空</Button>
                            </p>
                        </Grid>
                    </Grid>
                </Box >
            </Container >
            <div>
            {lenormands.map((item, index) => {
                        return (
                            <Grid container spacing={0} key={index} justifyContent="center" >
                                {items(item)}
                            </Grid>
                        )
                    })}
            </div>


        </Layout>
    );
}

export default Lenormand