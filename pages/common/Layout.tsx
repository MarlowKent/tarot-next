import ButtonAppBar from './ButtonAppBar';
import { makeStyles } from '@material-ui/styles';


const useStyles = makeStyles({
    root: {
        display: 'flex',
        height: '100%',
        overflow: 'hidden',
        width: '100%'
    },
    wrapper: {
        display: 'flex',
        flex: '1 1 auto',
        overflow: 'hidden',
    },
    contentContainer: {
        display: 'flex',
        flex: '1 1 auto',
        overflow: 'hidden',
    },
    contentArea: {
        flex: '1 1 auto',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
    },
    commonFrame: {
        marginTop: '80px',
        height: 'calc(100% - 80px)',
        overflow: 'auto'
    }
});

function Layout({ children }) {
    const classes = useStyles();
    return (<div className={classes.root}>

        <ButtonAppBar></ButtonAppBar>
        <div className={classes.wrapper}>
            <div className={classes.contentContainer}>
                <div className={classes.contentArea}>
                    <div className={classes.commonFrame}>
                        {children}
                    </div>

                </div>
            </div>
        </div>

        <style jsx global>{`
          html {
            width: 100%;
            height: 100%;
          }
          body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
          }
  
          #__next {
            width: 100%;
            height: 100%;
          }
        `}</style>
    </div>)


}

export default Layout