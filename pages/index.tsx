
import { makeStyles } from '@material-ui/styles';
import Layout from './common/Layout';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';

const useStyles = makeStyles({
  titleContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
},
});

function HomePage() {
  const classes = useStyles();
  return (<Layout>
            <Container maxWidth="lg">
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} className={classes.titleContent} >
                            <h1 className="jumbotron-heading">傑克塔羅</h1>
                            <p className="lead text-muted">本網站提供線上塔羅占卜，點選左側選單開始測算</p>
                        </Grid>
                    </Grid>
                </Box >
            </Container >
  </Layout>
  )

}

export default HomePage